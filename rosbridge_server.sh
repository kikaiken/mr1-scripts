#!/bin/bash
WORK_DIR="/home/kikaiken/scripts"
source ${WORK_DIR}/env_setup.sh

rosrun rosbridge_server rosbridge_udp >> ${WORK_DIR}/log/rosbridge_server.log 2>&1 &
