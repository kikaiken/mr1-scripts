#!/bin/bash

BRIDGE=br0

IP_ADDRESS=192.168.0.88/24

ETH_NIC=enp0s25
WIFI_NIC=wlx1cc0350205a1


brctl addbr $BRIDGE
brctl stp $BRIDGE off

ip addr add $IP_ADDRESS dev $BRIDGE

ip addr flush dev $ETH_NIC
brctl addif $BRIDGE $ETH_NIC

ip link set dev $ETH_NIC promisc on
ip link set dev $ETH_NIC up


#ip addr flush dev $WIFI_NIC
#ip link set dev $WIFI_NIC promisc on
#ip link set dev $WIFI_NIC up

ip link set dev $BRIDGE up
