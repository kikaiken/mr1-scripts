#!/bin/bash
bash network/start_bridge.sh >& bridge.log &
hostapd wifi/ap.conf >& ap.log &

roscore >& roscore.log &
rosrun rosbridge_server rosbridge_udp >& rodbridge_server.log &
